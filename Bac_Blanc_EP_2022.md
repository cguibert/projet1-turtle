## EXERCICE 1


On considère des mots à trous : ce sont des chaînes de caractères contenant uniquement
des majuscules et des caractères `*`. 

Par exemple `INFO*MA*IQUE`, `***I***E**` et `*S*` sont des mots à trous.  

Programmer une fonction `correspond` qui :

- prend en paramètres deux chaînes de caractères `mot` et `mot_a_trous` où
`mot_a_trous` est un mot à trous comme indiqué ci-dessus, 
- renvoie :
    -  `True`  si  on  peut  obtenir  `mot`  en  remplaçant  convenablement  les
      caractères `'*'` de `mot_a_trous`.
    - `False` sinon.

Exemple :

```python
>>> correspond('INFORMATIQUE', 'INFO*MA*IQUE')
True
>>> correspond('AUTOMATIQUE', 'INFO*MA*IQUE')
False
```

On fera figurer des tests unitaires à l'aide notamment de la commande `assert`.


## EXERCICE 2

On considère la fonction `insere` ci-dessous qui prend en argument un entier `a` et une liste `nombres` d'entiers triés par ordre croissant. Cette fonction insère la valeur `a` dans la liste.

```python
def insere(a, nombres):
    nombres.append(a)
    i = ...
    while a < ... and i >= 0: 
      nombres[i + 1] = ...
      nombres[i] = a
      i = ...
	return None
```

Compléter la fonction `insere` ci-dessus.

### Exemples:

    ```python
    >>> ex_1 = [1, 2, 4, 5]
    >>> insere(3, ex_1)
    >>> ex_1
    [1, 2, 3, 4, 5]
    ```

    ```python
    >>> ex_2 = [1, 2, 7, 12, 14, 25]
    >>> insere(7, ex_2)
    >>> ex_2
    [1, 2, 7, 7, 12, 14, 25]
    ```

    ```python
    >>> ex_3 = [2, 3, 4]
    >>> insere(1, ex_3)
    >>> ex_3
    [1, 2, 3, 4]
    ```

On pourra créer des tests unitaires  à l'aide notamment de la commande `assert`.
