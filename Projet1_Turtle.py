import turtle
import time
from typing import Tuple
from random import randint, shuffle

# Alias de types pour clarifier le code
Octet = int
Couleur = Tuple[Octet, Octet, Octet]
Coord = int


# constantes imposées
NIVEAU_MAX = 4
LARGEUR = 140
HAUT_NIV = 60
TAILLE_FEN = 30
LARG_PORTE = 30
NB_BAR = 5 # nb barreaux balcon


def ini_tortue(nb_maisons: int) -> None:
    """
    Place la tortue pour le début du dessin
    """
    turtle.setup(1.2*nb_maisons*LARGEUR, NIVEAU_MAX*1.2*HAUT_NIV)
    turtle.speed(0)
    turtle.hideturtle()

def couleur_aleatoire() -> Couleur:
    """
    Renvoie un triplet d'octets pour définir une couleur.
    """
    return (randint(0, 255), randint(0, 255), randint(0, 255))

def trait(x1: Coord, y1: Coord, x2:Coord ,y2: Coord) -> None:
    '''
    Réalise un trait partant des coordonnées (x1, y1) pour aller aux coordonnées (x2, y2)

    Paramètres
        x1, y1 : coordonnées du début du trait
        x2, y2 : coordonnées de la fin du trait
    '''
    turtle.up()
    turtle.goto(x1,y1)
    turtle.down()
    turtle.goto(x2,y2)

def rectangle(x: Coord, y: Coord, w: Coord, h: Coord) -> None:
    '''


    Paramètres
        x, y : coordonnées du centre de la base de rectangle
        w : largeur du rectangle
        h : hauteur du rectangle
    '''
    trait(x, y, x + w/2, y)
    trait(x + w/2, y, x + w/2, y + h)
    trait(x + w/2, y + h, x - w/2,y + h)
    trait(x - w/2, y + h, x - w/2, y)
    trait(x - w/2, y, x, y)

def fenetre(x:Coord, y:Coord) -> None:
    '''
    Paramètres :
        x est l'abcisse du centre de la fenêtre
        y est l'ordonnée du sol du niveau de la fenetre
    '''
    turtle.fillcolor("White")
    turtle.begin_fill()
    rectangle(x, y, TAILLE_FEN, TAILLE_FEN)
    turtle.end_fill()

def porte1(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte rectangulaire
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    rectangle(x, y, LARG_PORTE, 50)
    turtle.end_fill()

def porte2(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte arondie
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    trait(x, y, x + 15, y)
    trait(x + 15, y, x + 15,y + 40)
    turtle.setheading(90)
    turtle.circle(15, 180)
    trait(x - 15, y + 40,x - 15, y)
    trait(x - 15, y, x, y)
    turtle.end_fill()

def fenetre_balcon(x:Coord, y:Coord) -> None:
    '''
    Paramètres :
        x est l'abcisse du centre de la porte-fenetre-balcon
        y est l'ordonnée du sol du niveau de la porte-fenetre-balcon
    '''
    turtle.fillcolor("White")
    turtle.begin_fill()
    rectangle(x, y, LARG_PORTE, TAILLE_FEN + HAUT_NIV // 4) # tracé de la porte fenêtre
    turtle.end_fill()
    for i in range(NB_BAR-1): # tracé des barreaux
      rectangle(x, y, (LARG_PORTE * (7 - i * 2) // 5), TAILLE_FEN)

def toit1(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Réalise un toit fait de 4 traits

    Toit 2 pentes
    Paramètres :
        x : abcisse du centre du toit
        y_sol : ordonnée du sol du la rue
        niveau : num du niveau (0 pour les rdc, ...)
    '''
    turtle.fillcolor("Black")
    turtle.begin_fill()
    trait(x, y_sol + HAUT_NIV * niveau, x + LARGEUR/2, y_sol + HAUT_NIV * niveau) # 1e moitié du sol du toit
    trait(x + LARGEUR/2, y_sol+ HAUT_NIV * niveau, x, y_sol + HAUT_NIV * niveau + 30) # 1e pente du toit
    trait(x, y_sol + HAUT_NIV * niveau + 30, x - LARGEUR/2, y_sol + HAUT_NIV * niveau) # 2de pente du toit
    trait(x - LARGEUR/2, y_sol + HAUT_NIV * niveau, x, y_sol + HAUT_NIV * niveau) # 2de moitié du sol du toit
    turtle.end_fill()

def toit2(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Réalise un toit plat composé de deux traits et deux demi-cercles

    Toit plat
    Paramètres :
        x : abcisse du centre du toit
        y_sol : ordonnée du sol du la rue
        niveau : num du niveau (0 pour les rdc, ...)
    '''
    turtle.fillcolor("Black")
    turtle.begin_fill()
    trait(x, y_sol + HAUT_NIV * niveau, x + LARGEUR/2, y_sol + HAUT_NIV * niveau) # 1e moitié du sol du toit
    turtle.setheading(0)
    turtle.circle(5,180)
    trait(x + LARGEUR/2, y_sol + HAUT_NIV * niveau + 10, x - LARGEUR/2, y_sol + HAUT_NIV * niveau + 10) # partie haute du toit
    turtle.setheading(180)
    turtle.circle(5,180)
    trait(x - LARGEUR/2, y_sol + HAUT_NIV * niveau, x, y_sol + HAUT_NIV * niveau) # 2de moitié du sol du toit
    turtle.end_fill()

def rdc(x:Coord, y_sol:Coord, c_facade:Couleur, c_porte: Couleur) -> None:
    '''
    Réalise le rez-de-chaussée avec une façade de couleur aléatoire et de largeur constante LARG_PORTE, composé d'un rectangle, 
    d'une porte de type aléatoire (plate ou arrondi) et deux fenêtres.

    Paramètres
        x : (int) abscisse du centre
        y_sol : ordonnée du sol du la rue
        c_facade : couleur de la façade
        c_porte : couleur de la porte
    '''
    turtle.fillcolor(c_facade)
    turtle.begin_fill()
    rectangle(x, y_sol, LARGEUR, HAUT_NIV)
    turtle.end_fill()
    n = [0,0,1]
    shuffle(n)
    Lst = [LARGEUR//3, 0, -(LARGEUR//3)]
    for i in range(len(Lst)): # choix aléatoire du type de porte et de sa place, puis réalisation de celles-ci
      if n[i] == 0:
        fenetre(x - Lst[i], y_sol + HAUT_NIV//4)
      else:
        n2 = randint(0,1)
        if n2 == 0:
          porte1(x - Lst[i], y_sol, c_porte)
        else:
          porte2(x - Lst[i], y_sol, c_porte)
    
def etage(x:Coord, y_sol:Coord, couleur: Couleur, niveau: int) -> None:
    '''
    Réalise un étage de l'immeuble d'une couleur aléatoire identique à celle du rez-de-chaussée, ainsi que de 3 fenêtres, 
    chacune soit classique soit porte-fenêtre avec balcon.

    Paramètres
        x : abscisse du centre de l'étage
        y_sol : ordonnée du sol du la rue
        couleur : couleur de la façade de l'étage
        niveau : numéro de l'étage en partant de 0 pour le rdc
    '''
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    rectangle(x, y_sol + HAUT_NIV * niveau, LARGEUR, HAUT_NIV) # tracé de la façade de l'étage
    turtle.end_fill()
    Lst = [LARGEUR//3, 0, -(LARGEUR//3)]
    for elt in Lst: # choix aléatoire d'un type de fenêtre et réalisation de celles-ci
      n = randint(0, 1)
      if n == 0:
        fenetre(x - elt, y_sol + HAUT_NIV * niveau + HAUT_NIV//4)
      else:
        fenetre_balcon(x - elt, y_sol + HAUT_NIV * niveau)

def toit(x:Coord, y_sol:Coord, niveau:int) -> None:
    '''
    Choisit et réalise l'un des deux toits en noir (toit plat ou avec 2 pentes).

    Paramètres
        x : abscisse du centre de l'étage
        niveau : numéro de l'étage en partant de 0 pour le rdc
    '''
    n = randint(0, 1)
    if n == 0:
      toit1(x, y_sol, niveau)
    else:
      toit2(x, y_sol, niveau)


# ----- Immeuble -----

def immeuble(x:Coord, y_sol:Coord) -> None:
    '''
    Réalise un immeuble de la rue de couleur de porte et de façade aléatoire, ainsi que nombre d'étages aléatoire. 

    Paramètres
        x : abscisse du centre de l'étage
        y_sol : ordonnée du sol du la rue
    '''
    # Nombre d'étage (aléatoire)
    nb_etages = randint(0, NIVEAU_MAX)
    #Couleurs des éléments (aléatoire)
    couleur_facade = couleur_aleatoire()
    couleur_porte = couleur_aleatoire()
    # Dessin du RDC
    rdc(x, y_sol, couleur_facade, couleur_porte)
    # Dessin des étages
    niveau = 1
    while niveau <= nb_etages:
        etage(x, y_sol, couleur_facade,niveau)
        niveau = niveau + 1
    # Dessin du toit
    toit(x, y_sol, niveau)

# ----- Sol de la rue -----

def sol(y_sol:Coord, nb_maisons:int) -> Coord:
    '''
    Réalise le sol de la rue

    Paramètres
        y_sol : ordonnée du sol du la rue
    '''
    x_sol = LARGEUR//10 + (LARGEUR*1.2*nb_maisons)//2
    turtle.pensize(3)
    trait(-x_sol, y_sol, x_sol, y_sol)
    return x_sol



def dessin(nb_maisons:int) -> None:
    turtle.colormode(255)
    ini_tortue(nb_maisons)
    y_sol = -2 * HAUT_NIV
    # Dessin du sol de la rue
    x_sol = sol(y_sol, nb_maisons)
    # Dessin des immeubles
    for i in range(nb_maisons):
        immeuble(LARGEUR * 1.2//2 - x_sol + i * LARGEUR * 1.2, y_sol)
    time.sleep(5)

# ------------------------------
# ------------------------------
# ------------------------------

#if __name__ == "__main__":
n = int(input("\n nombre de maisons ?  "))
dessin(n)