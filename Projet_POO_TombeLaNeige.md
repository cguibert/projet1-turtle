# Projet TombeLaNeige:

TombeLaNeige est un projet POO que nous avons divisé en plusieurs parties.
L'objectif initial était de faire tomber de la neige, tout en utilisant des classes et le module p5 pour visualiser le programme,
puis d'explorer d'autres possibilités.



## Version initiale de notre programme POO:

Nous avons modifié le programme initial afin d'y implémenter deux classes Flocon et Neige. les objets de type Flocon contiennent 4 attributs privés pour leur abscisse et ordonnée, pour leur rayon ainsi pour leur vitesse verticale. Un objet Neige, quant à lui, contient les NB_FLOCONS flocons dans son attribut boules_de_neiges. à l'aide des fonctions setup et draw et de p5, les flocons vont se déplacer vers le bas indéfiniment, formant de la neige.

```
import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_FLOCONS = 42


class Flocon:

    def __init__(self) -> None:
        """
        crée un flocon avec ses 4 attributs _abscisse, _ordonnee, _rayon, _vitesse_y.
        """
        self._abscisse = uniform(0, LARGEUR)
        self._ordonnee = uniform(0, HAUTEUR)
        self._rayon = uniform(5, 20)
        self._vitesse_y = uniform(3, 15)
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse du flocon
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée du flocon
        """
        return(self._ordonnee)
    
    def add_ordonnee(self, val:int) -> None:
        """
        ajoute une valeur val à l'ordonnée du flocon
        """
        self._ordonnee += val    


class Neige:

    def __init__(self) -> None:
        """
        initialise une liste contenant tous les flocons.
        """
        self.boules_de_neiges = [Flocon() for i in range(NB_FLOCONS)]

    def descendre(self, f: Flocon) -> None:
        """
        dessine le flocon dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def get_boules_de_neiges(self) -> list:
        """
        renvoie la liste des flocons.
        """
        return self.boules_de_neiges

truc = Neige()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_FLOCONS):
        truc.descendre(truc.get_boules_de_neiges()[i])

if __name__ == "__main__":
    p5.run()
```
![](/Capture_d_écran_de_2021-11-19_11-54-25.png)

Résultat obtenu.
Nous ne nous sommes pas arrêté là !

## Version 2.0:

Sur cette version, nous avons ajouté une vitesse horizontale et fait les modifications nécessaires pour que certaines boules de neige partent en diagonale. Pour cela, nous avons rajouté dans la classe Neige une fonction diagonale.

```
import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_FLOCONS = 42


class Flocon:

    def __init__(self) -> None:
        """
        crée un flocon avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(0, LARGEUR)
        self._ordonnee = uniform(0, HAUTEUR)
        self._rayon = uniform(5, 20)
        self._vitesse_x = uniform(0,5)
        if self._vitesse_x > 0:
            self._vitesse_y = uniform(7, 15)
        else:
            self._vitesse_y = uniform(3, 6)
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse du flocon
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée du flocon
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse du flocon
        """
        self._abscisse += val
    
    def add_ordonnee(self, val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée du flocon
        """
        self._ordonnee += val
    
    

class Neige:

    def __init__(self) -> None:
        """
        initialise une liste contenant tous les flocons.
        """
        self.boules_de_neiges = [Flocon() for i in range(NB_FLOCONS)]

    def descendre(self, f: Flocon) -> None:
        """
        dessine le flocon dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def diagonale(self, f: Flocon) -> None:
        """
        dessine le flocon dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._abscisse = 0
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def get_boules_de_neiges(self) -> list:
        """
        renvoie la liste des flocons.
        """
        return self.boules_de_neiges

truc = Neige()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_FLOCONS):
        if truc.get_boules_de_neiges()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_boules_de_neiges()[i])
        else:
            truc.descendre(truc.get_boules_de_neiges()[i])

if __name__ == "__main__":
    p5.run()
```

![](/Capture_d_écran_de_2021-11-19_11-54-25.png)

Résultat obtenu pour cette 2e version !

## Version 3.0:

pour la 3e version, nous avons fait évoluer les boules de neige en boules de billard. Nous avons modifié les conditions faisant remonter les boules de neige en haut quand elle touchait le sol, ou les faisant revenir à gauche quand elle touchait la bordure droite, afin qu'ici elles rebondissent et repartent dans une autre direction. Nous les avons, dans un souci de réalisme, faites toutes de même taille et, surtout, au nombre de 15.

```
import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_BILLES = 15


class Bille:

    def __init__(self) -> None:
        """
        crée une bille avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(0, LARGEUR)
        self._ordonnee = uniform(0, HAUTEUR)
        self._rayon = 20
        self._vitesse_x = uniform(0,5)
        if self._vitesse_x > 0:
            self._vitesse_y = uniform(7, 15)
        else:
            self._vitesse_y = uniform(3, 6)
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse de la bille
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée de la bille
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse de la bille
        """
        self._abscisse += val
    
    def add_ordonnee(self,val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée de la bille
        """
        self._ordonnee += val
    

class Billard:

    def __init__(self) -> None:
        """
        initialise une liste contenant toutes les billes
        """
        self.boules_de_billard = [Bille() for i in range(NB_BILLES)]

    def descendre(self, f: Bille) -> None:
        """
        dessine la bille dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._vitesse_y = -f._vitesse_y

    def diagonale(self, f: Bille) -> None:
        """
        dessine la bille dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._vitesse_x = -f._vitesse_x
        if f._ordonnee > HAUTEUR:
            f._vitesse_y = -f._vitesse_y
        if f._abscisse < 0:
            f._vitesse_x = -f._vitesse_x
        if f._ordonnee < 0:
            f._vitesse_y = -f._vitesse_y


    def get_boules_de_billard(self) -> list:
        """
        renvoie la liste des billes.
        """
        return self.boules_de_billard

truc = Billard()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_BILLES):
        if truc.get_boules_de_billard()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_boules_de_billard()[i])
        else:
            truc.descendre(truc.get_boules_de_billard()[i])
if __name__ == "__main__":
    p5.run()
```

![](/Capture_d_écran_de_2021-11-19_12-09-24.png)

Boules de billard obtenues !

## Version 4.0:

Pour cette dernière version, nous avons pour le coup transformer les flocons en poissons dans l'objectif de les faire nager en banc. Nous définissons, pour ce faire, aléatoirement des attributs _vitesse_x et _vitesse_y uniques et communs à tous les poissons. il se déplace ainsi en banc, plus rapproché, grâce à des abscisse et coordonnée toujours aléatoires mais plus restrictives.

```
import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_POISSONS = 42
vx, vy = uniform(3,15), uniform(3,15)
limites_abs, limites_ord = [200, 600], [300, 500]

class Poisson:

    def __init__(self) -> None:
        """
        crée un poisson avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(limites_abs[0], limites_abs[1])
        self._ordonnee = uniform(limites_ord[0], limites_ord[1])
        self._rayon = uniform(5, 20)
        self._vitesse_x = vx
        self._vitesse_y = vy
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse du poisson
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée du poisson
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse du poisson
        """
        self._abscisse += val
    
    def add_ordonnee(self, val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée du poisson
        """
        self._ordonnee += val
    
    

class Banc:

    def __init__(self) -> None:
        """
        initialise une liste contenant tous les poissons.
        """
        self.banc_de_poissons = [Poisson() for i in range(NB_POISSONS)]

    def descendre(self, f: Poisson) -> None:
        """
        dessine le poisson dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def diagonale(self, f: Poisson) -> None:
        """
        dessine le poisson dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._abscisse = 0
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def get_banc_de_poissons(self) -> list:
        """
        renvoie la liste des poissons.
        """
        return self.banc_de_poissons

truc = Banc()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_POISSONS):
        if truc.get_banc_de_poissons()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_banc_de_poissons()[i])
        else:
            truc.descendre(truc.get_banc_de_poissons()[i])

if __name__ == "__main__":
    p5.run()
```

![](/Capture_d_écran__622_.png)

Résultat obtenu pour cette dernière version !

