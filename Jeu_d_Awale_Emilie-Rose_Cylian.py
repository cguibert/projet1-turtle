"""
Le jeu d'Awalé
"""

"""
On va utiliser une classe tablier et une classe trou.
"""

from random import *    # On aura besoin des fonctions de random pour l'ia.
import time

class Trou:

    def __init__(self, i):
        self.les_graines = ["0" for i in range(4)]
        self.nb_graines = len(self.les_graines)
        self.index = i

    def _print_(self) -> int:
        return(self.nb_graines)



class Tablier:

    def __init__(self):
        self.les_trous = [Trou(i) for i in range(12)]
        
    def graines_joueur(self) -> int:
        nb = 0
        for i in range(6):
            nb += self.les_trous[i].nb_graines
        return nb
    
    def graines_ia(self) -> int:
        nb = 0
        for i in range(6, 12): # bizarre
            nb += self.les_trous[i].nb_graines
        return nb



def Initialisation():
    """
    Initialisation du tablier de jeu.
    """
    T = Tablier()
    return T


def print_graines(T):
    """
    Fonction d'affichage des graines.
    Exécutée à chaque tour.
    """
    lst_joueur, lst_ia = list(), list()
    for trou in T.les_trous:
        if trou.index <= 5:
            lst_joueur += [trou._print_()]
        else:
            lst_ia += [trou._print_()]
    return(f"Les graines de l'adversaire : {lst_ia}, vos graines : {lst_joueur}")


def enlever_graines(trou_où_enlever):
    """
    Enlever les graines d'un trou
    Utilisé au début du tour pour récupérer les graines du trou choisi par le joueur,
    puis en fin de tour pour récupérer les graines dans les trous adverses suivant les règles du jeu.
    """
    gs = trou_où_enlever.les_graines
    trou_où_enlever.les_graines = []
    trou_où_enlever.nb_graines = 0
    return(gs)


def placer_graine(g, trou_où_ajouter):
    """
    Placer une graine dans un trou
    """
    trou_où_ajouter.les_graines += g
    trou_où_ajouter.nb_graines += 1


def déplacer_graines(sens_de_rotation, trou, T):
    """
    Enlever les graines d'un trou choisi, puis les replacer une par une dans les trous suivants.
    """
    gs = enlever_graines(trou)
    index = trou.index
    for elt in gs:
        if index < 11:
            index += sens_de_rotation
        else:
            index = 0
        placer_graine(elt, T.les_trous[index])
    return(index)


def reprendre_graines(T, index, camp, sens_de_rotation, total):
    """
    Reprendre les graines de certains trous adverses selon les règles du jeu.
    """
    while index in camp and T.les_trous[index].nb_graines in [2, 3]:
        gs = enlever_graines(T.les_trous[index])
        total += len(gs)
        index -= sens_de_rotation
    return(total)


def peut_on_continuer(T, total_ia, total_joueur, temps_restant):
    """
    Avant chaque duo de tour, test pour vérifier que les conditions de la partie permettent de la continuer encore,
    selon les règles du jeu.
    """
    if temps_restant <= 0:
        return False
    elif T.graines_joueur() + T.graines_ia() <= 1:
        return False
    elif total_ia >= 25 or total_joueur >= 25:
        return False
    elif (T.graines_ia() == 0 and T.graines_joueur() < 4) or (T.graines_joueur() == 0 and T.graines_ia() < 4):
        return False
    else:
        return True




def fin_graines(T, total, camp):
    """
    En fin de partie,
    Récupérer les graines restantes et les compter pour les ajouter au total
    du dernier joueur à avoir jouer.
    """
    for i in range(camp[0]-1, camp[-1]-1):
        gs = enlever_graines(T.les_trous[i])
        total += gs
    return total






def jeu_d_Awale():
    """
    Fonction exécutant, gérant l'avancée du jeu.
    """
    T = Initialisation()
    sens_de_rotation = 1
    tps_joueur = int(input(" quel niveau de difficulté choisissez-vous ? Taper 180 pour Débutant, 120 pour Intermédiaire ou 50 pour Expert."))
    while tps_joueur not in [180, 120, 50]:
        tps_joueur = int(input("Ce n'est pas un temps valide ! Taper 180, 120 ou 50."))
    camp_joueur, camp_ia = [1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12]
    total_joueur, total_ia = 0, 0
    print(print_graines(T))


    while peut_on_continuer(T, total_joueur, total_ia, tps_joueur):
        

        # Tour du joueur
        print(int(tps_joueur))
        tps_start = time.time()
        index = int(input("Quel case voulez-vous vider ? entrer un numéro de 1 à 6"))
        print(index, "tour")
        tps = time.time() - tps_start
        tps_joueur -= tps
        while index not in [1,2,3,4,5,6]:
            index = int(input("Ce n'est pas un indice valide ! Entrer un numéro entre 1 et 6. PAF !"))
        dernier_index = déplacer_graines(sens_de_rotation, T.les_trous[index-1], T)
        print(index, "reprendre")
        total_joueur = reprendre_graines(T, dernier_index, camp_ia, sens_de_rotation, total_joueur)


        print(total_joueur, "total")
        
        
        print(print_graines(T))


        # Tour de l'IA
        index = randint(7, 12)
        dernier_index = déplacer_graines(sens_de_rotation, T.les_trous[index-1], T)
        total_ia = reprendre_graines(T, dernier_index, camp_joueur, sens_de_rotation, total_ia)
        print(total_ia, "total ia")

        
        print(print_graines(T))


    if T.graines_joueur() == 0:
        total_final = fin_graines(T, total_ia, camp_ia)
    elif T.graines_ia() == 0:
        total_final = fin_graines(T, total_joueur, camp_joueur)


    if total_joueur > total_ia:
        print(f"WINNER ! Vous avez au total {total_joueur} graines, l'ia n'en a que {total_ia} !")
    elif total_ia == total_joueur:
        print(f"MATCH NUL ! Vous et l'ia avez {total_joueur} graines !")
    else:
        print(f"LOOSER ! L'ia a au total {total_ia} graines, vous n'en avez que {total_joueur} !")



if __name__ == "__main__":
    """
    ttt = Trou(12)
    ttt._print_()
    print(ttt.index, ttt.les_graines, ttt.nb_graines)
    T = Initialisation()
    print(T.graines_ia())
    print(T.graines_ia())
    print(print_graines(T))
    """
    jeu_d_Awale()