# Codage de Huffmann: (Objectif Bac)
## 1.
a.
Il est nécessaire d'au maximum 4 valeurs pour coder une lettre, il faut donc 4 bits par lettre.

b.
On multiplie le nombre de bits par lettre par le nombrede caractères:
1000 * 4 = 4000
Puis on divise par 8 pour obtenir un nombre d'octets:
4000 / 8 = 500
Il faudrait donc 500 octets pour coder un message de 1000 caractères dans cet alphabet.

## 2.
Pour chaque caractère, il faut utiliser un séquence de 4 chiffres binaires. Ainsi, on pourrait avoir :
A : 0001
B : 0010
C : 0011
D : 0100
E : 0101
F : 0110
G : 0111
H : 1000

## 3.
a)
Pour **CACHE**, on a C : $000$, A : $10$, H : $1111$ et E : $01$.
Ainsi, **CACHE** va être coder **00010000111101**.

b)
le code **001101100111001** va correspondre à **BADGE**, avec B : $001$, A : $10$, D : $1100, G : $1110$ et E : $01$.

## 4.
a)
Puisque le code proposé à la question 3 est de taille fixe égale à 4, le message fera 4 * 1000 = 4000 bits.

b) 
Il suffit ici d'ajouter la longueur de chaque lettre multipliée par son nombre d'occurences dans le message.
On a donc :
240 × 2 + 140 × 3 + 160 × 3 + 51 × 4 + 280 × 2 + 49 × 4 + 45 × 4 + 35 × 4 = **2660**
Le message fera **2660** bits.

## 5.
a)
7 étapes, ou fusions d'arbre, sont nécessaires à la création de l'arbre.

b)
On obtient l'arbre suivant:

![](/63DD404E-0005-47BC-B350-AEE5CE80A8EE.jpeg)


## 6.
a)
On obtient le résultat suivant :

![](/E1AB0C8D-ECC1-4663-8F40-19C3987CDB67.jpeg)

b) le code de **F** est ici **1101**, ce qui correspond au code du tableau.

## 7.

~~~python
import bisect

class ArbreHuffman:

    def __init__(self, lettre, nbocc, g=None, d=None):
        self.lettre = lettre
        self.nbocc = nbocc
        self.gauche = g
        self.droite = d

    def est_feuille(self) -> bool:
        return self.droite is None and self.gauche is None

    def __lt__(self, other):
        # Un arbre A est strictement inférieur à un arbre B
        # si le nombre d'occurrences indiqué dans A est
        # strictement inférieur à celui de B
        return -self.nbocc < -other.nbocc


def parcours(arbre, chemin_en_cours, dico):
    if arbre is None:
        return
    if arbre.est_feuille():
        dico[arbre.lettre] = chemin_en_cours
    else:
        parcours(arbre.gauche, chemin_en_cours + [0], dico)
        parcours(arbre.droite, chemin_en_cours + [1], dico)
    return dico

def fusionne(gauche, droite) -> ArbreHuffman:
    nbocc_total = gauche.nbocc + droite.nbocc
    return ArbreHuffman(None, nbocc_total, gauche, droite)


def compte_occurrences(texte: str) -> dict:
    """
    Renvoie un dictionnaire avec chaque caractère du texte
    comme clé, et le nombre d'apparition de ce caractère
    dans le texte en valeur

    >>> compte_occurrences("AABCECA")
    {"A": 3, "B": 1, "C": 2, "E": 1}
    """
    occ =  dict()
    for car in texte:
        if car not in occ:
            occ[car] = 1
        occ[car] = occ[car] + 1
    return occ

def construit_liste_arbres(texte: str) -> list:
    """
    Renvoie une liste d'arbres de Huffman, chacun réduit
    à une feuille
    """
    dic_occurrences = compte_occurrences(texte)
    liste_arbres = []
    for lettre, occ in dic_occurrences.items():
        liste_arbres.append(ArbreHuffman(lettre, occ))
    return liste_arbres


def codage_huffman(texte: str) -> dict:
    """
    Codage de Huffman optimal à partir d'un texte

    >>> codage_huffman("AAAABBBBBCCD")
    {'A': [0, 0], 'C': [0, 1, 0], 'D': [0, 1, 1], 'B': [1]}
    """

    liste_arbres = construit_liste_arbres(texte)
    # Tri par nombre d'occurrences décroissant
    liste_arbres.sort()
    # Tant que tous les arbres n'ont pas été fusionnés
    while len(liste_arbres) > 1:
        # Les deux plus petits nombres d'occurrences
        # sont à la fin de la liste
        droite = liste_arbres.pop()
        gauche = liste_arbres.pop()
        new_arbre = fusionne(gauche, droite)
        # Le module bisect permet d'insérer le nouvel
        # arbre dans la liste, de manière à ce que la
        # list reste triée
        bisect.insort(liste_arbres, new_arbre)
    # Il ne reste plus qu'un arbre dans la liste,
    # c'est notre arbre de Huffman
    arbre_huffman = liste_arbres.pop()
    # Parcours de l'arbre pour relever les codes
    dico = {}
    parcours(arbre_huffman, [], dico)
    return dico

# Script principal
with open("texte.txt") as f:
    texte = f.read()
print(codage_huffman(texte))
~~~


