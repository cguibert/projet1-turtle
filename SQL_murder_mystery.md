SELECT * FROM crime_scene_report 
WHERE type = 'murder' 
AND city = 'SQL City';

SELECT * FROM person WHERE address_street_name = "Northwestern Dr"
ORDER BY address_number DESC LIMIT 1

SELECT * FROM person WHERE name LIKE "Annabel%"
AND address_street_name = "Franklin Ave";

SELECT * 
FROM income 
JOIN person 
  ON income.ssn = person.ssn 
ORDER BY annual_income DESC LIMIT 10

SELECT P.name, transcript FROM interview AS I 
JOIN person AS P
ON P.id = I.person_id
WHERE P.name = "Morty Schapiro" or P.name = "Annabel Miller";

SELECT DISTINCT membership_status FROM get_fit_now_member

SELECT DISTINCT check_in_date FROM get_fit_now_check_in

SELECT membership_id FROM get_fit_now_check_in WHERE check_in_date = "20180109";

SELECT * FROM get_fit_now_check_in AS G
JOIN get_fit_now_member AS M
ON G.membership_id = M.id
WHERE G.check_in_date = "20180109"

SELECT * FROM get_fit_now_check_in AS G
JOIN get_fit_now_member AS M
ON G.membership_id = M.id
WHERE G.check_in_date = "20180109" AND LOWER(M.membership_status) = "gold"

SELECT * FROM get_fit_now_check_in AS G
JOIN get_fit_now_member AS M
ON G.membership_id = M.id
WHERE G.check_in_date = "20180109" AND M.id LIKE "48Z%"

SELECT * FROM get_fit_now_check_in AS G
JOIN get_fit_now_member AS M
ON G.membership_id = M.id
JOIN person AS P
ON P.id = M.person_id
JOIN drivers_license AS D
ON P.license_id = D.id
WHERE G.check_in_date = "20180109" AND M.id LIKE "48Z%"
AND D.plate_number LIKE "%H42W%"

***Jeremy Bowers***

SELECT P.name, transcript FROM interview AS I 
JOIN person AS P
ON P.id = I.person_id
WHERE P.name = "Jeremy Bowers"

SELECT P.name, D.car_model, D.height, Fb.date, Fb.event_name FROM person AS P
JOIN drivers_license AS D
ON P.license_id = D.id
JOIN facebook_event_checkin AS Fb
ON Fb.person_id = P.id
WHERE D.hair_color = "red" AND D.car_model = "Model S" AND D.height BETWEEN "65" AND "67"

***Miranda Priestly***
