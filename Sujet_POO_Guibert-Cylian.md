# Exercice 2: (POO)
## 1)
a. Les deux assertions à prévoir sont
```
assert arome in ['abricot','fraise','vanille','aucun'], "La valeur dans l'attribut arome n'est pas acceptable."
```
et
```
assert duree >= 0, "la durée entrée n'est pas correcte."
```
La première renverra une erreur si l'arôme rentrée n'est pas dans la liste des arômes possibles.
La seconde renverra une erreur si la durée est inférieur à 0.

b. La valeur 'aromatise' sera affecté à self.__genre, puisque 'fraise' représente une des valeurs 
correspondant à un yaourt aromatisée.

c. La fonction GetArome peut être la suivante :
```
def GetArome(self) -> str:
    return(self.__arome)
```

## 2)
La fonction SetArome peut se présenter comme suit, avec notamment un test levant une erreur
si arome n'est pas dans la liste des valeurs acceptables.
```
def SetArome(self, arome: str) -> None:
    if arome not in ['abricot','fraise','vanille','aucun']:
        raise ValueError
    self._arome = arome
    __SetGenre(self, arome)
```
## 3)
a. empiler(p, Yaourt) peut s'écrire ainsi :
On ajoute Yaourt à la fin de p, puis on renvoie p.
```
def empiler(p, Yaourt):
    p.pile.append(Yaourt)
    return(p.pile)
```

b. depiler(p) peut s'écrire de la manière suivante :
```
def depiler(p):
    return(p.pile[-1] if not estVide(p))
```

c. on peut écrire la fonction estVide comme suit :
```
def estVide(p) -> bool:
    return(p.pile == [])
```

d. 
```
mon_Yaourt1 = Yaourt('aucun',18)
mon_Yaourt2 = Yaourt('fraise',24)
ma_pile = creer_pile()
empiler(ma_pile, mon_Yaourt1)
empiler(ma_pile, mon_Yaourt 2)
print(depiler(ma_pile).GetDuree())
print(estVide(ma_pile))
```
Le bloc de commandes suivant créé d'abord deux Yaourts: mon_Yaourt1 et mon_Yaourt2.
Il créé ensuite une pile ma_pile et y empile les deux yaourts créés précédemment.
Enfin, il affiche la date de durabilité minimale du Yaourt en haut de la pile, 
soit duree de mon_Yaourt2, ainsi que estVide(ma_pile).
**Il renvoie donc 24, puis False.**


