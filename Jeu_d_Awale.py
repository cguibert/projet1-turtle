"""
Le jeu d'Awalé
"""

"""
On va utiliser une classe tablier, une classe trou et une classe graine.
"""

class Trou:
    def __init__(self, i):
        self.les_graines = ["0" for i in range(4)]
        self.nb_graines = len(self.les_graines)
        self.index = i

class Tablier:
    def __init__(self):
        self.les_trous = [Trou(i) for i in range(1,13)]




def Initialisation():
    T = Tablier()
    return T

def enlever_graines(trou_où_enlever):
    gs = trou_où_enlever.les_graines
    trou_où_enlever.les_graines = []
    trou_où_enlever.nb_graines = 0
    return(gs)

def placer_graine(g, trou_où_ajouter):
    trou_où_ajouter.les_graines.append(g)
    trou_où_ajouter.nb_graines += 1

def déplacer_graines(sens_de_rotation, trou, T):
    gs = enlever_graines(trou)
    index = trou.index
    for elt in gs:
        index += sens_de_rotation
        placer_graine(elt, T.les_trous[index])
    return(index)

def reprendre_graines(T, index, camp):
    

def jeu_d_Awale(sens_de_rotation, joueur_début):
    T = Initialisation()
    sens_de_rotation = input("quel sens de rotation voulez-vous ? 1 pour le sens des aiguilles d'une montre et -1 pour le sens contraire ")
    joueur_début = input("Qui débute ? vous ou IA")
    camp_joueur, camp_ia = [1, 6], [7,12] 
    while 