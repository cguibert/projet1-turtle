####################################################################
##
## Cylian
##
#####################################################################



#############
# EXERCICE I
#############


# fonction à créer
def correspond(mot: str, mot_a_trous: str) -> bool:
  """
  La fonction parcourt la liste en cherchant une incohérence, qui invaliderait la correspondance des deux mots. 
  La fonction renvoie False dès qu'elle en trouve, et renvoie True si elle a pu parcourir le mot sans problème.
  """
  if len(mot) != len(mot_a_trous):    #On règle le cas où mot et mot_a_trous sont de tailles différentes.
    return False    #S'ils ne font pas la même taille, on peut tout de suite conclure que les deux chaines de caractères ne peuvent pas correspondre, et donc renvoyer False.
  for i in range(len(mot)):   #On va parcourir toute la longueur des deux chaines de caractère.
    if mot_a_trous[i] != mot[i] and mot_a_trous[i] != "*":    #Si la lettre d'indice i dans mot_a_trous n'est ni égale à la lettre d'indice i dans mot, ni une astérisque,
      return False    #alors cela signifie qu'on ne peut pas faire correspondre les deux mots. On renvoie False.
  return True   #On a pu parcourir mot en entier sans trouver d'incohérence, on peut donc faire correspondre les deux mots. On renvoie True.


# tests à créer
"""
On teste le cas d'un couple de mots correspondant, d'un couple de mots complètement différents, 
d'un mot_a_trous avec un astérisque au début et à la fin, et celui de deux mots de longueur différente.
"""
assert correspond('INFORMATIQUE', 'INFO*MA*IQUE')
assert not(correspond('AUTOMATIQUE', 'INFO*MA*IQUE'))
assert correspond('INFORMATIQUE', '*NFORMATIQU*')
assert not(correspond('INFORMATIQUE', 'INFORMATI*'))


####################################################################


#############
# EXERCICE II
#############

# fonction à compléter

def insere(a, nombres):
    nombres.append(a)   #On ajoute a à la fin de nombres.
    i = len(nombres) - 2    #On initialise i comme la longueur de la liste - 2, car le dernier indice de la liste est len(nombres)-1,
    #et car on ne veut pas comparer a avec lui-même, qu'on vient d'insérer à la fin de nombres.
    while a < nombres[i] and i >= 0: #On parcourt la liste dans l'ordre décroissant, tant que la valeur d'indice i n'est pas inférieur à a et tant qu'on n'a pas fini de parcourir nombres.
      nombres[i + 1] = nombres[i]   #On décale la valeur d'indice i au cran suivant.
      nombres[i] = a    #On intercale a en indice i.
      i = i - 1   #On soustrait 1 à i
    return None


# tests à créer
"""
On teste les cas du sujet, en y ajoutant le cas d'une valeur a maximale dans la liste.
"""
ex_1 = [1, 2, 4, 5]
insere(3, ex_1)
assert ex_1 == [1, 2, 3, 4, 5]

ex_2 = [1, 2, 7, 12, 14, 25]
insere(7, ex_2)
assert ex_2 == [1, 2, 7, 7, 12, 14, 25]

ex_3 = [2, 3, 4]
insere(1, ex_3)
assert ex_3 == [1, 2, 3, 4]

ex_4 = [1, 2, 3, 4, 8]
insere(9, ex_4)
assert ex_4 == [1, 2, 3, 4, 8, 9]