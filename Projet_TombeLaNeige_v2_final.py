import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_FLOCONS = 42


class Flocon:

    def __init__(self) -> None:
        """
        crée un flocon avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(0, LARGEUR)
        self._ordonnee = uniform(0, HAUTEUR)
        self._rayon = uniform(5, 20)
        self._vitesse_x = uniform(0,5)
        if self._vitesse_x > 0:
            self._vitesse_y = uniform(7, 15)
        else:
            self._vitesse_y = uniform(3, 6)
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse du flocon
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée du flocon
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse du flocon
        """
        self._abscisse += val
    
    def add_ordonnee(self, val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée du flocon
        """
        self._ordonnee += val
    
    

class Neige:

    def __init__(self) -> None:
        """
        initialise une liste contenant tous les flocons.
        """
        self.boules_de_neiges = [Flocon() for i in range(NB_FLOCONS)]

    def descendre(self, f: Flocon) -> None:
        """
        dessine le flocon dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def diagonale(self, f: Flocon) -> None:
        """
        dessine le flocon dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._abscisse = 0
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def get_boules_de_neiges(self) -> list:
        """
        renvoie la liste des flocons.
        """
        return self.boules_de_neiges

truc = Neige()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_FLOCONS):
        if truc.get_boules_de_neiges()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_boules_de_neiges()[i])
        else:
            truc.descendre(truc.get_boules_de_neiges()[i])

if __name__ == "__main__":
    p5.run()
