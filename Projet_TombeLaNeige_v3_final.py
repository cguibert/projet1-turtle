import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_BILLES = 15


class Bille:

    def __init__(self) -> None:
        """
        crée une bille avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(0, LARGEUR)
        self._ordonnee = uniform(0, HAUTEUR)
        self._rayon = 20
        self._vitesse_x = uniform(0,5)
        if self._vitesse_x > 0:
            self._vitesse_y = uniform(7, 15)
        else:
            self._vitesse_y = uniform(3, 6)
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse de la bille
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée de la bille
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse de la bille
        """
        self._abscisse += val
    
    def add_ordonnee(self,val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée de la bille
        """
        self._ordonnee += val
    

class Billard:

    def __init__(self) -> None:
        """
        initialise une liste contenant toutes les billes
        """
        self.boules_de_billard = [Bille() for i in range(NB_BILLES)]

    def descendre(self, f: Bille) -> None:
        """
        dessine la bille dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._vitesse_y = -f._vitesse_y

    def diagonale(self, f: Bille) -> None:
        """
        dessine la bille dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._vitesse_x = -f._vitesse_x
        if f._ordonnee > HAUTEUR:
            f._vitesse_y = -f._vitesse_y
        if f._abscisse < 0:
            f._vitesse_x = -f._vitesse_x
        if f._ordonnee < 0:
            f._vitesse_y = -f._vitesse_y


    def get_boules_de_billard(self) -> list:
        """
        renvoie la liste des billes.
        """
        return self.boules_de_billard

truc = Billard()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_BILLES):
        if truc.get_boules_de_billard()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_boules_de_billard()[i])
        else:
            truc.descendre(truc.get_boules_de_billard()[i])
if __name__ == "__main__":
    p5.run()
