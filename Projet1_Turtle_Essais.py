import turtle
import time
from typing import Tuple
from random import randint, shuffle

# Alias de types pour clarifier le code
Octet = int
Couleur = Tuple[Octet, Octet, Octet]
Coord = int


# constantes imposées
NIVEAU_MAX = 4
LARGEUR = 140
HAUT_NIV = 60
TAILLE_FEN = 30
LARG_PORTE = 30
NB_BAR = 5 # nb barreaux balcon


def ini_tortue(nb_maisons: int) -> None:
    """
    Place la tortue pour le début du dessin
    """
    turtle.setup(1.2*nb_maisons*LARGEUR, NIVEAU_MAX*1.2*HAUT_NIV)
    turtle.speed(0)
    turtle.hideturtle()

def couleur_aleatoire() -> Couleur:
    """
    Renvoie un triplet d'octets pour définir une couleur.
    """
    return (randint(0, 256), randint(0, 256), randint(0, 256))

def trait(x1: Coord, y1: Coord, x2:Coord ,y2: Coord) -> None:
    '''
    Paramètres
        x1, y1 : coordonnées du début du trait
        x2, y2 : coordonnées de la fin du trait
    '''
    turtle.up()
    turtle.goto(x1,y1)
    turtle.down()
    turtle.goto(x2,y2)

def rectangle(x: Coord, y: Coord, w: Coord, h: Coord) -> None:
    '''
    Paramètres
        x, y : coordonnées du centre de la base de rectangle
        w : largeur du rectangle
        h : hauteur du rectangle
    '''
    trait(x,y,x+w/2,y)
    trait(x+w/2,y,x+w/2,y+h)
    trait(x+w/2,y+h,x-w/2,y+h)
    trait(x-w/2,y+h,x-w/2,y)
    trait(x-w/2,y,x,y)

def fenetre(x:Coord, y:Coord) -> None:
    '''
    Paramètres :
        x est l'abcisse du centre de la fenêtre
        y est l'ordonnée du sol du niveau de la fenetre
    '''
    rectangle(x, y, TAILLE_FEN, TAILLE_FEN)

def porte1(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte rectangulaire
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    rectangle(x,y,30,50)
    turtle.end_fill()

def cercle(x,y):
  turtle.up()
  turtle.goto(x,y)
  turtle.down()
  turtle.circle(100,180)

def porte2(x:Coord, y:Coord, couleur:Couleur) -> None:
    '''
    Porte arondie
    Paramètres :
        x est l'abcisse du centre de la porte
        y est l'ordonnée du sol du niveau de la porte
        couleur : couleur de la porte
    '''
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    trait(x,y,x+15,y)
    trait(x+15,y,x+15,y+50)
    turtle.left(90)
    turtle.circle(15,180)
    trait(x-15,y+50,x-15,y)
    trait(x-15,y,x,y)
    turtle.end_fill()

if __name__ == "__main__":
  porte1(0,0,couleur_aleatoire())
