import p5
from random import uniform

LARGEUR, HAUTEUR = 1000, 800
NB_POISSONS = 42
vx, vy = uniform(3,15), uniform(3,15)
limites_abs, limites_ord = [200, 600], [300, 500]

class Poisson:

    def __init__(self) -> None:
        """
        crée un poisson avec ses 5 attributs _abscisse, _ordonnee, _rayon, _vitesse_y et _vitesse_x.
        """
        self._abscisse = uniform(limites_abs[0], limites_abs[1])
        self._ordonnee = uniform(limites_ord[0], limites_ord[1])
        self._rayon = uniform(5, 20)
        self._vitesse_x = vx
        self._vitesse_y = vy
  
    def get_abs(self) -> int: 
        """
        renvoie l'abscisse du poisson
        """
        return(self._abscisse)

    def get_ord(self) -> int:
        """
        renvoie l'ordonnée du poisson
        """
        return(self._ordonnee)

    def add_abscisse(self, val: int) -> None:
        """
        ajoute une valeur val à l'abscisse du poisson
        """
        self._abscisse += val
    
    def add_ordonnee(self, val: int) -> None:
        """
        ajoute une valeur val à l'ordonnée du poisson
        """
        self._ordonnee += val
    
    

class Banc:

    def __init__(self) -> None:
        """
        initialise une liste contenant tous les poissons.
        """
        self.banc_de_poissons = [Poisson() for i in range(NB_POISSONS)]

    def descendre(self, f: Poisson) -> None:
        """
        dessine le poisson dans son état actuel, puis modifie son ordonnée.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f._ordonnee += f._vitesse_y
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def diagonale(self, f: Poisson) -> None:
        """
        dessine le poisson dans son état actuel, puis modifie son ordonnée et son abscisse.
        """
        p5.circle((f._abscisse, f._ordonnee), f._rayon)
        f.add_abscisse(f._vitesse_x)
        f.add_ordonnee(f._vitesse_y)
        if f._abscisse > LARGEUR:
            f._abscisse = 0
        if f._ordonnee > HAUTEUR:
            f._ordonnee = 0

    def get_banc_de_poissons(self) -> list:
        """
        renvoie la liste des poissons.
        """
        return self.banc_de_poissons

truc = Banc()

def setup():
    p5.size(LARGEUR, HAUTEUR)
        
def draw():
    p5.background(0)
    for i in range(NB_POISSONS):
        if truc.get_banc_de_poissons()[i]._vitesse_x != 0:
            truc.diagonale(truc.get_banc_de_poissons()[i])
        else:
            truc.descendre(truc.get_banc_de_poissons()[i])

if __name__ == "__main__":
    p5.run()
